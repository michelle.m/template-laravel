<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view("page.form");
    }

    public function berhasil(request $request){
        $firstname = $request->nama1;
        $lastname = $request->nama2;
        $jeniskelamin = $request->jk;
        return view('page.welcome', compact('firstname', 'lastname', 'jeniskelamin'));
    }
}
